procedure for producing new iteration scripts
=============================================
- see `PROCEDURE` file


iterations directory files
==========================

`qt_mask`: mask out specific channels using qt bitshift file
- this is done by filtering the qt bit shift files through a regex after
  they've been produced in an iteration
- usage: `qt_mask [iterationdir]`
- BE CAREFUL since this script will overwrite the original bit shift file(s)

`ibase`: baseline iterations with
- large cells set to -1500
- small cells set to 0xC0
- all gains set to 1
- all bit shifts set to 0

`i[iteration#]`: calibration iteration
- corresponding correction files:
  - `FmsCorr.heppel.txt_i[iteration#]`: requested gains in Steve's format
  - `FmsCorr.txt_i[iteration#]`: requested gains in Thomas's format

`working`: sandbox directory


run 17 iterations
=================
- `i0_2017` -- first iteration using `V vs. eta` curves
  - applied day 44
  - `V vs. eta` curves determined from `i8_2015` using the script
    `2015_iterations/voltage_vs_eta.C`; the curves are stored in
    `i0_2017/V_vs_eta{large,small}.pdf`
    - large cells vary between 1350V-1600V
    - small cells vary between 0x9B-0xFF


- `i1_2017` = `i0_2017` + `FmsCorr.txt_i1`
  - based on day 52-53 data
  - applied day 053
  - voltage limits:
    - all fermi bases limited to -1600V
    - all large "resistive" bases limited to -1800V
      - additionally, large resistive channels 130, 132, 135 set to -1600V and bitshift=0
    - all large psu bases limited to -1700V (-1600V if bitshift>1)
    - all small cells limited to the range `0x10 - 0xF0`
  - `0 <= bitshift <= +1` enforced


- `i1_2017_disableRussianCells` = `i0_2017` + `FmsCorr.txt_i1` + `Russian Cells V=0x00 bs=-5`
  - same as `i1_2017` but with Russian small cells "disabled"; voltage set to `0x00` and 
    bitshift set to `-5`; this was done to emphasize the Yale cells during triggering, because
    so far we are having problems seing reasonable ADC counts in the outer corners of the small
    cells
  - IN PRACTICE, we only loaded the bitshifts of -5 file and not applied the `0x00` voltage settings


- `i2_2017` = `i1_2017` + `FmsCorr.txt_i2`
  - based on day 54-55 data
  - applied day 055
  - voltage limits:
    - all fermi bases limited to -1600V
    - all large "resistive" bases limited to -1800V
      - additionally, large resistive channels 130, 132, 135 set to -1600V and bitshift=0
    - all large psu bases limited to -1700V (-1600V if bitshift>1)
    - all small cells limited to the range `0x00 - 0xFF` (no limits)
  - `-4 <= bitshift <= +4` enforced


- `i3_2017` = `i2_2017` + `FmsCorr.txt_i3`
  - based on day 56 data
  - applied day 057
  - voltage limits:
    - all fermi bases limited to -1600V
    - all large "resistive" bases limited to -1800V
      - additionally, large resistive channels 130, 132, 135 set to -1600V and bitshift=0
    - all large psu bases limited to -1700V (-1600V if bitshift>1)
    - all small cells limited to the range `0x00 - 0xFF` (no limits)
  - `-4 <= bitshift <= +4` enforced


- `i3_2017_bs2` = `i3_2017` but with bitshift limit -2 to +4
- `i3_2017_bs35` = `i3_2017` but with bitshift limit -3 to +5


- `i4_2017` = `i3_2017` + `FmsCorr.txt_i4`
  - based on day 57-58 data
  - applied day 059
  - voltage limits:
    - all fermi bases limited to -1600V
    - all large "resistive" bases limited to -1800V
      - additionally, large resistive channels 130, 132, 135 set to -1600V and bitshift=0
    - all large psu bases limited to -1700V (-1600V if bitshift>1)
    - all small cells limited to the range `0x00 - 0xFF` (no limits)
  - `-3 <= bitshift <= +5` enforced


- `i2_2016` copied over from fmsvoltages2016
  - applied day 60
  - documentation from `fmsvoltages2016`:
    - `i2_2016` = `i1_2016` + `FmsCorr.txt_i2`
    - carl's (2015/2016) gain goals from run15 implemented
    - all fermi bases limited to -1600V
    - all large "resistive" bases limited to -1800V
      - resistive channels 130, 132, 135 set to -1600V and bitshift=0
    - all large psu bases limited to -1700V (-1600V if bitshift>1)
    - `-5 <= bitshift <= +5`
    - qt masks (following convention of `qt*_tac.dat` files, 
      change `-1` to `4095`):
      - `crt3 slot0 chan31`
      - `crt3 slot1 chan31`
      - `crt3 slot2 chan3`
      - `crt4 slot3 chan24`
      - `crt4 slot10 chan10`


- `i5_2017` = `i2_2016` + `FmsCorr.txt_i5`
  - based on day 63 data (acquired using 2016 voltage settings)
  - applied day 065
  - voltage limits:
    - all large cell bases now have lower limit of -900 V
    - all fermi bases limited to -1600V
    - all large "resistive" bases limited to -1800V
      - additionally, large resistive channels 130, 132, 135 set to -1600V and bitshift=0
    - all large psu bases limited to -1700V (-1600V if bitshift>1)
    - all small cells limited to the range `0x00 - 0xFF` (no limits)
  - `0 <= bitshift <= +5` enforced
  - because this was based on 2016 data, some further problems occured which
    demanded some work-arounds:
    - when `askedChange==1`, program just takes old values
      and does not call `optimise`; I've commented that part out
      and now optimise is always executed
      - otherwise negative bitshifts from previous iterations would remain
      - some "dead" cells are also kept "dead"
        - forcing "dead" large cells to 1400 with bitshift 0:
          - 3 cells which were stacked last (north 9,10,28)
          - south channel 474 
    - temporarily turned on `BITSHIFT_SHORT_CIRCUIT`, but just the part
      that activates if shift<0; if the cell has bitshift<0, 
      it will set bitshift to zero and the cell voltage
      to the lowest allowed voltage
    - disabled `LOCKED_BITSHIFTS` since it was locking the `-5` bitshifts
      at `-5`
    - removed leftover `i2_2016` qt data offsets manually
    - turned `BITSHIFT_SHORT_CIRCUIT` back off after `i5_2017` produced


- `i6_2017` = `i5_2017_hts` + `FmsCorr.txt_i6`
  - based on day 65 data 
  - applied day 066
  - note: suffix `_hts` on `i5_2017` means `i5_2017` updated to include
    the Hot Tower Suppressions
  - voltage limits:
    - all large cell bases now have lower limit of -900 V
    - all fermi bases limited to -1600V
    - all large "resistive" bases limited to -1800V
      - additionally, large resistive channels 130, 132, 135 set to -1600V and bitshift=0
    - all large psu bases limited to -1700V (-1600V if bitshift>1)
    - all small cells limited to the range `0x00 - 0xFF` (no limits)
  - `0 <= bitshift <= +5` enforced


- `i7_2017` = `i6_2017_hts` + `FmsCorr.txt_i7`
  - based on day 66 data (acquired using 2016 voltage settings)
  - applied day 068
  - includes ADC offsets on 
    - `qt3 1 30`
    - `qt3 1 31`
  - voltage limits:
    - all large cell bases now have lower limit of -900 V
    - all fermi bases limited to -1600V
    - all large "resistive" bases limited to -1800V
      - additionally, large resistive channels 130, 132, 135 set to -1600V and bitshift=0
    - all large psu bases limited to -1700V (-1600V if bitshift>1)
    - all small cells limited to the range `0x00 - 0xFF` (no limits)
  - `0 <= bitshift <= +5` enforced

 
- `i8_2017` = `i7_2017_hts` + `FmsCorr.txt_i8`
  - based on day 69 data
  - applied day 071
  - includes ADC offsets on 
    - `qt2 1 17`
    - `qt3 1 30`
    - `qt3 1 31`
    - `qt4 10 10`
  - voltage limits:
    - all large cell bases now have lower limit of -900 V
    - all fermi bases limited to -1600V
    - all large "resistive" bases limited to -1800V
      - additionally, large resistive channels 130, 132, 135 set to -1600V and bitshift=0
    - all large psu bases limited to -1700V (-1600V if bitshift>1)
    - all small cells limited to the range `0x00 - 0xFF` (no limits)
  - `-5 <= bitshift <= +5` enforced


- `i9_2017` = `i8_2017_hts` + `FmsCorr.txt_i9`
  - based on day 76 data (run 18076023)
  - applied day 082
  - includes ADC offsets a few more channels, plus "stomping" of QT channels
    which are not connected to a cell (see `*.dat` files)
  - voltage limits:
    - all large cell bases now have lower limit of -900 V
    - all fermi bases limited to -1600V
    - all large "resistive" bases limited to -1800V
      - additionally, large resistive channels 130, 132, 135 set to -1600V and bitshift=0
    - all large psu bases limited to -1700V (-1600V if bitshift>1)
    - all small cells limited to the range `0x00 - 0xFF` (no limits)
  - `-5 <= bitshift <= +5` enforced
  - Steve will provide a list of manual bitshift and voltage settings to change, since we
    prefer not to have negative bitshifts (viz. those <-2)


- `i10_2017` = `i9_2017_hts` + `FmsCorr.txt_i10`
  - based off of day 100-110 data
  - applied day 114
  - voltage limits:
    - all large cell bases now have lower limit of -900 V
    - all fermi bases limited to -1600V
    - all large "resistive" bases limited to -1800V
      - additionally, large resistive channels 130, 132, 135 set to -1600V and bitshift=0
    - all large psu bases limited to -1700V (-1600V if bitshift>1)
    - all small cells limited to the range `0x00 - 0xFF` (no limits)
  - `-5 <= bitshift <= +5` enforced
