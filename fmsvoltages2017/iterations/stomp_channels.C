// "stomps" on certain QT channels by setting their
// offsets to 4095 and bitshifts to -5 (or otherwise stated)
// -- all unused QT channels are fully stomped (4095,-5)
// -- additional channels may be added to the list with
//    custom offsets and bitshifts

void stomp_channels(TString iteration_dir = "sandbox_tmp") {

  // define custom channels to "stomp"  [crate][bsslot][bschan][offset][bs]
  /*
  const Int_t N_EXTRA = 11;
  Int_t extra_channels[N_EXTRA][5] = {
    {2, 1, 17, 4095, 0},
    {2, 2, 2, 4095, 0},
    {2, 2, 12, 4095, 0},
    {3, 1, 30, 4095, 0},
    {3, 1, 31, 4095, 0},
    {4, 2, 19, 4095, 0},
    {4, 2, 22, 4095, 0},
    {4, 2, 30, 4095, 0},
    {4, 3, 29, 4095, 0},
    {4, 5, 19, 4095, -5},
    {4, 10, 10, 4095, 0}
  };
  */
  ///*
  // (don't override any)
  const Int_t N_EXTRA = 1;
  Int_t extra_channels[N_EXTRA][5] = { {1000, 1000, 1000, 4095, 0} };
  //*/
  
  // read cell geometry tree geotr
  TFile * geofile = new TFile("geotr.root","READ");
  TTree * geotr = (TTree*) geofile->Get("geotr");
  Int_t NCELLS = geotr->GetEntries();
  Int_t qtcrate,qtslot,qtcard,qtchan;
  geotr->SetBranchAddress("qtcrate",&qtcrate);
  geotr->SetBranchAddress("qtslot",&qtslot);
  geotr->SetBranchAddress("qtcard",&qtcard);
  geotr->SetBranchAddress("qtchan",&qtchan);

  // read LUT files
  const Int_t N = 4;
  TTree * luttree[N]; 
  TTree * luttree_new[N];
  TString lutfile_n[N];
  TString lutfile_new_n[N];
  Int_t bsslot[4];
  Int_t bschan[4];
  Int_t offset[4];
  Int_t bs[4];
  Int_t bsslot_new[4];
  Int_t bschan_new[4];
  Int_t offset_new[4];
  Int_t bs_new[4];
  TH2F * hist_offset[4];
  TH2F * hist_offset_new[4];
  TH2F * hist_bs[4];
  TH2F * hist_bs_new[4];
  TString hist_offset_n[4];
  TString hist_offset_new_n[4];
  TString hist_bs_n[4];
  TString hist_bs_new_n[4];
  for(int q=0; q<N; q++) {
    lutfile_n[q] = Form("%s/qt%d_tac.dat",iteration_dir.Data(),q+1);
    lutfile_new_n[q] = Form("%s.new",lutfile_n[q].Data());
    //lutfile_n[q] = Form("%s/qt%d_tac.dat.cooled",iteration_dir.Data(),q+1);
    //lutfile_new_n[q] = Form("%s.stomped",lutfile_n[q].Data());

    luttree[q] = new TTree();
    luttree[q]->ReadFile(lutfile_n[q],"bsslot/I:bschan/I:offset/I:bs/I");
    luttree[q]->SetBranchAddress("bsslot",&(bsslot[q]));
    luttree[q]->SetBranchAddress("bschan",&(bschan[q]));
    luttree[q]->SetBranchAddress("offset",&(offset[q]));
    luttree[q]->SetBranchAddress("bs",&(bs[q]));

    hist_offset_n[q] = Form("original_offsets_in_QT%d",q+1);
    hist_offset_new_n[q] = Form("new_offsets_in_QT%d",q+1);
    hist_bs_n[q] = Form("original_bitshifts_in_QT%d",q+1);
    hist_bs_new_n[q] = Form("new_bitshifts_in_QT%d",q+1);

    hist_offset[q] = new TH2F(
      hist_offset_n[q].Data(),hist_offset_n[q].Data(),
      11,0,11,32,0,32);
    hist_offset_new[q] = new TH2F(
      hist_offset_new_n[q].Data(),hist_offset_new_n[q].Data(),
      11,0,11,32,0,32);
    hist_bs[q] = new TH2F(
      hist_bs_n[q].Data(),hist_bs_n[q].Data(),
      11,0,11,32,0,32);
    hist_bs_new[q] = new TH2F(
      hist_bs_new_n[q].Data(),hist_bs_new_n[q].Data(),
      11,0,11,32,0,32);

    hist_offset[q]->GetXaxis()->SetTitle("QTslot-1");
    hist_offset[q]->GetYaxis()->SetTitle("8*QTcard+QTchan");
    hist_offset_new[q]->GetXaxis()->SetTitle("QTslot-1");
    hist_offset_new[q]->GetYaxis()->SetTitle("8*QTcard+QTchan");
    hist_bs[q]->GetXaxis()->SetTitle("QTslot-1");
    hist_bs[q]->GetYaxis()->SetTitle("8*QTcard+QTchan");
    hist_bs_new[q]->GetXaxis()->SetTitle("QTslot-1");
    hist_bs_new[q]->GetYaxis()->SetTitle("8*QTcard+QTchan");

    hist_offset[q]->SetMinimum(-1);
    hist_offset[q]->SetMaximum(4096);
    hist_offset_new[q]->SetMinimum(-1);
    hist_offset_new[q]->SetMaximum(4096);

    hist_bs[q]->SetMinimum(-5);
    hist_bs[q]->SetMaximum(5);
    hist_bs_new[q]->SetMinimum(-5);
    hist_bs_new[q]->SetMaximum(5);
  };

  // scan through each LUT file and stomp
  Int_t qtslot_conv,qtcard_conv,qtchan_conv;
  Bool_t exists;
  Int_t offset_st, bs_st;
  TString sign;
  TString spacer;
  Int_t binn;
  for(int q=0; q<N; q++) {
    printf("stomping qt%d channels...\n",q+1);
    for(int x=0; x<luttree[q]->GetEntries(); x++) {
      luttree[q]->GetEntry(x);

      // convert bs qt coords to regular qt coords
      qtslot_conv = bsslot[q] + 1;
      qtcard_conv = (Int_t) (bschan[q] / 8);
      qtchan_conv = bschan[q] % 8;

      // loop through geotr to see if channel exists
      exists = false;
      for(int g=0; g<NCELLS; g++) {
        geotr->GetEntry(g);
        if(qtcrate == q+1) {
          if(qtslot == qtslot_conv &&
             qtcard == qtcard_conv &&
             qtchan == qtchan_conv) {
            exists = true;
            break;
          };
        };
      };

      // set offsets and bs's accordingly
      if(!exists) {
        offset_st = 4095;
        bs_st = -5;
      } else {
        offset_st = offset[q];
        bs_st = bs[q];
      };

      // override new offsets and bs's using extra_channels list
      for(int e=0; e<N_EXTRA; e++) {
        if(q+1 == extra_channels[e][0]) {
          if(bsslot[q] == extra_channels[e][1] &&
             bschan[q] == extra_channels[e][2]) {
            offset_st = extra_channels[e][3];
            bs_st = extra_channels[e][4];
          };
        };
      };

      // bitshift sign and spacer
      sign = " ";
      if(bs_st>0) sign = "+";
      else if(bs_st<0) sign = "";
      spacer = bschan[q]<10 ? " ":"";


      // output
      gSystem->RedirectOutput(lutfile_new_n[q].Data(),(x==0)?"w":"a");
      printf("%d %s%d %d %s%d\n",
        bsslot[q],spacer.Data(),bschan[q],
        offset_st,sign.Data(),bs_st);
      gSystem->RedirectOutput(0);


      // fill hists
      binn = hist_offset[q]->FindBin(bsslot[q],bschan[q]);
      hist_offset[q]->SetBinContent(binn,offset[q]);
      binn = hist_bs[q]->FindBin(bsslot[q],bschan[q]);
      hist_bs[q]->SetBinContent(binn,bs[q]);

    }; // eo for x in luttree


    // now read in the newly created lut table and fill hists
    luttree_new[q] = new TTree();
    luttree_new[q]->ReadFile(lutfile_new_n[q],"bsslot/I:bschan/I:offset/I:bs/I");
    luttree_new[q]->SetBranchAddress("bsslot",&(bsslot_new[q]));
    luttree_new[q]->SetBranchAddress("bschan",&(bschan_new[q]));
    luttree_new[q]->SetBranchAddress("offset",&(offset_new[q]));
    luttree_new[q]->SetBranchAddress("bs",&(bs_new[q]));

    for(int y=0; y<luttree_new[q]->GetEntries(); y++) {
      luttree_new[q]->GetEntry(y);
      binn = hist_offset_new[q]->FindBin(bsslot_new[q],bschan_new[q]);
      hist_offset_new[q]->SetBinContent(binn,offset_new[q]);
      binn = hist_bs_new[q]->FindBin(bsslot_new[q],bschan_new[q]);
      hist_bs_new[q]->SetBinContent(binn,bs_new[q]);
    };
  }; // eo for q 0 to 3
  printf("done stomping\n");


  // draw hists
  gStyle->SetOptStat(0);
  TCanvas * canv = new TCanvas("canv","canv",1000,1000);
  canv->Divide(2,2);
  for(int q=0; q<N; q++) {
    canv->cd(1); 
      hist_offset[q]->Draw("colz");
      hist_offset[q]->Draw("textsame");
    canv->cd(2); 
      hist_offset_new[q]->Draw("colz");
      hist_offset_new[q]->Draw("textsame");
    canv->cd(3); 
      hist_bs[q]->Draw("colz");
      hist_bs[q]->Draw("textsame");
    canv->cd(4); 
      hist_bs_new[q]->Draw("colz");
      hist_bs_new[q]->Draw("textsame");
    if(q==0) canv->Print("stomp.pdf(","pdf");
    else if(q+1==N) canv->Print("stomp.pdf)","pdf");
    else canv->Print("stomp.pdf","pdf");
  };
  printf("check stomp.pdf to make sure it was done correctly\n");
};
