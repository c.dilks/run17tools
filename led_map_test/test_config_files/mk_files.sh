#!/bin/bash

for i in {1..32}; do 
  fname="test${i}.txt"
  echo "LED Module System Event File: Test Event File For File IO Diagnostic" > $fname
  echo "Number of Events:" >> $fname
  echo "1" >> $fname
  echo "Event 1 Data:" >> $fname
  let p=$i+32
  echo $i $p
  for x in {1..64}; do
    if [ $x -eq $i -o $x -eq $p ]; then
      echo 255 >> $fname
    else
      echo 0 >> $fname
    fi
  done
done
